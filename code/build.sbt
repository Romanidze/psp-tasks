val projectVersion = "1.0"
val projectName = "psp-tasks"
val projectScalaVersion = "2.13.2"

lazy val commonSettings = Seq(
  version := projectVersion,
  scalaVersion := projectScalaVersion,
  resolvers ++= Seq(Resolver.mavenCentral, Resolver.mavenLocal),
  scalacOptions := Seq("-unchecked", "-feature", "-deprecation", "-encoding", "utf8"),
  scalafmtOnCompile := true
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .settings(name := s"$projectName")
  .aggregate(
    firstTask,
    secondTask,
    thirdTask,
    fourthTask,
    sixthTask,
    seventhTask,
    eighthTask,
    ninethTask,
    tenthTask
  )
  .dependsOn(
    firstTask,
    secondTask,
    thirdTask,
    fourthTask,
    sixthTask,
    seventhTask,
    eighthTask,
    ninethTask,
    tenthTask
  )

lazy val firstTask = (project in file("modules/first"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-first",
    libraryDependencies ++= Dependencies.firstTaskDeps
  )

lazy val secondTask = (project in file("modules/second"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-second",
    libraryDependencies ++= Dependencies.secondTaskDeps
  )

lazy val thirdTask = (project in file("modules/third"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-third",
    libraryDependencies ++= Dependencies.thirdTaskDeps
  )

lazy val fourthTask = (project in file("modules/fourth"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-fourth",
    libraryDependencies ++= Dependencies.fourthTaskDeps
  )

lazy val sixthTask = (project in file("modules/sixth"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-sixth",
    libraryDependencies ++= Dependencies.sixthTaskDeps
  )

lazy val seventhTask = (project in file("modules/seventh"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-seventh",
    libraryDependencies ++= Dependencies.seventhTaskDeps
  )

lazy val eighthTask = (project in file("modules/eighth"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-eighth",
    libraryDependencies ++= Dependencies.eighthTaskDeps
  )

lazy val ninethTask = (project in file("modules/nineth"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-nineth",
    libraryDependencies ++= Dependencies.ninethTaskDeps
  )

lazy val tenthTask = (project in file("modules/tenth"))
  .settings(commonSettings)
  .settings(
    name := s"$projectName-tenth",
    libraryDependencies ++= Dependencies.tenthTaskDeps
  )