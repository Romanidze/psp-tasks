package com.romanidze.psp.sixth.files

import java.io.BufferedReader
import java.util.stream.Collectors

import better.files.File

import scala.jdk.CollectionConverters._

object FilesOperations {

  def getFile(fileName: String): File = {

    val inputFile: File = File(fileName)

    if (inputFile.exists) {
      println("file with such name already exists. please, write another name")
      System.exit(1)
    }

    inputFile.createFile()

  }

  def readFile(file: File): Vector[String] = {

    val br: BufferedReader = file.newBufferedReader

    val result: Vector[String] = br
      .lines()
      .collect(Collectors.toList[String]())
      .asScala
      .toVector

    br.close()

    result

  }

  def writeToFile(file: File, input: String): Unit = {

    val elements: Array[String] = input.split(" ")
    val counterValue: String = elements(0)
    val numbers: Array[Double] =
      elements
        .drop(1)
        .map(_.toDouble)
        .filter(elem => (elem > 1.0) && (elem < 10.0))

    if (numbers.length == counterValue.toInt) {
      val pw = file.newPrintWriter()
      pw.println(numbers.mkString(" "))
      pw.close()
    }

  }

}
