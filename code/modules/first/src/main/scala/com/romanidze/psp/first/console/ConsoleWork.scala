package com.romanidze.psp.first.console

import java.util.concurrent.atomic.AtomicReference

import better.files.File
import com.romanidze.psp.first.files.FilesOperations

object ConsoleWork {

  def launch(): Unit = {

    val scanner = new java.util.Scanner(System.in)

    print("Write file name: ")

    val fileName: String = scanner.nextLine()

    val fileInput: File = FilesOperations.getFile(fileName)

    println()
    print("Please, write the operation to perform (available: read, write, quit): ")

    val atomicOperation: AtomicReference[String] = new AtomicReference[String]()
    atomicOperation.set(scanner.nextLine())

    while (!atomicOperation.get().equals("quit")) {

      if (atomicOperation.get().equals("read")) {

        val numbers: Vector[String] = FilesOperations.readFile(fileInput)
        numbers.foreach(println)

      } else if (atomicOperation.get().equals("write")) {

        print("Please, write the input string with numbers to write: ")

        val writeInput: String = scanner.nextLine()

        FilesOperations.writeToFile(fileInput, writeInput)

      }

      print("Please, write next command of what to do: ")

      atomicOperation.set(scanner.nextLine())

      println()

    }

    println("Thank you for using such program")

  }

}
