package com.romanidze.psp.first.files

import java.io.BufferedReader
import java.util.stream.Collectors

import better.files.File

import scala.jdk.CollectionConverters._

object FilesOperations {

  def getFile(fileName: String): File = {

    val inputFile: File = File(fileName)

    if (!inputFile.exists) {
      inputFile.createFile()
    }

    inputFile

  }

  def readFile(file: File): Vector[String] = {

    val br: BufferedReader = file.newBufferedReader

    val result: Vector[String] = br
      .lines()
      .collect(Collectors.toList[String]())
      .asScala
      .toVector

    br.close()

    result

  }

  def writeToFile(file: File, input: String): Unit = {

    val elements: Array[String] = input.split(" ")
    val counterValue: String = elements(0)
    val numbers = elements.drop(1)

    if (numbers.length == counterValue.toInt) {
      val pw = file.newPrintWriter()
      numbers.foreach(pw.println)
      pw.close()
    }

  }

}
