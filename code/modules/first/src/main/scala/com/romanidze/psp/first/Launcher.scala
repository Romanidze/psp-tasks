package com.romanidze.psp.first

import com.romanidze.psp.first.console.ConsoleWork

object Launcher extends App {
  ConsoleWork.launch()
}
