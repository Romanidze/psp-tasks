package com.romanidze.psp.nineth

import java.nio.file.Paths

import better.files.File

import scala.collection.mutable.ListBuffer

object Launcher extends App {

  val keyWords: Seq[String] = Seq("val", "var", "def", "+", "-", "*", "/", "return")

  val functions: Seq[String] = Seq(
    "min",
    "max",
    "filter",
    "flatten",
    "diff",
    "intersect",
    "union",
    "map",
    "flatMap",
    "foldLeft",
    "foldRight",
    "drop",
    "head",
    "mkString"
  )

  def calculateLOC(input: File): Int = {

    val wordsList: ListBuffer[String] = ListBuffer.empty[String]

    val stringIter: Iterator[String] = input.lineIterator
    stringIter.foreach(elem => wordsList += elem.trim)

    val finalLines: List[String] = wordsList.filter(!_.isEmpty).toList

    val keywordCounter: Int = finalLines.foldLeft(0)((counter, inputStr) => {

      if (keyWords.contains(inputStr)) {
        counter + 1
      } else {
        counter
      }

    })

    val functionsCounter: Int = finalLines.foldLeft(0)((counter, inputStr) => {

      if (functions.contains(inputStr)) {
        counter + 1
      } else {
        counter
      }

    })

    keywordCounter + functionsCounter

  }

  val scanner = new java.util.Scanner(System.in)

  print("Write path to analyse: ")

  val filePath: String = scanner.nextLine()
  println()

  print("Write estimated Object LOC (space - separated): ")

  val estimatedLOC: String = scanner.nextLine()
  println()

  val convertedLOC: List[Int] =
    estimatedLOC
      .split(" ")
      .map(_.toInt)
      .toList

  val inputDir = File(Paths.get(filePath))

  val files: List[File] = inputDir.list.toList

  if (convertedLOC.length != files.length) {
    println("wrong estimated object")
  }

  val rawLocList: List[Int] = files.map(calculateLOC)

  print("Write desired num to sort by: ")

  val desiredNum: Int = scanner.nextInt()
  println()

  print("Write the operation to sort by (< or >): ")

  val desiredOperation: String = scanner.nextLine()
  println()

  val locList: Seq[Int] = desiredOperation match {

    case "<" => rawLocList.sortBy(_ < desiredNum)
    case ">" => rawLocList.sortBy(_ > desiredNum)
    case _   => rawLocList.sortBy(_ < desiredNum)

  }

  println(s"sorted list: $locList")

  val countVar: Int = files.length

  val firstUpElem: Int =
    convertedLOC.zip(locList).foldLeft(0)((counter, elem) => counter + elem._1 * elem._2)

  val secondUpElem: Int =
    countVar * (convertedLOC.sum / convertedLOC.length) * (locList.sum / locList.length)

  val squared: Int = (locList.sum / locList.length) * (locList.sum / locList.length)

  val downElem =
    locList.foldLeft(0)((counter, elem) => counter + (elem * elem - countVar * squared))

  val b1: Double = (firstUpElem - secondUpElem).toDouble / downElem.toDouble

  val b0: Double =
    (convertedLOC.sum / convertedLOC.length).toDouble - b1 * (locList.sum / locList.length)

  println(s"b0 = ${b0}, b1 = ${b1}")

  val varianceSum: Double = convertedLOC
    .zip(locList)
    .foldLeft(0.0)((counter, elem) => {
      counter + (elem._1 - b0 - b1 * elem._2) * (elem._1 - b0 - b1 * elem._2)
    })

  val variance: Double = (1 / (countVar - 2)) * varianceSum
  val stdDev: Double = math.sqrt(variance)

  val t1 = 0.85 // 70%
  val t2 = 0.95 // 90%

  val xK: Int = locList.sum
  val yK: Int = convertedLOC.sum

  val xAvg: Int = (locList.sum / locList.length)

  val rangeDescrim: Int = locList.foldLeft(0)((counter, elem) => {
    counter + (elem - xAvg) * (elem - xAvg)
  })

  val range1 =
    t1 * stdDev * math.sqrt(1 + (1 / countVar) + ((xK - xAvg) * (xK - xAvg) / rangeDescrim))

  val range2 =
    t2 * stdDev * math.sqrt(1 + (1 / countVar) + ((xK - xAvg) * (xK - xAvg) / rangeDescrim))

  println(s"70% : LPI is ${yK - range1}, UPI is ${yK + range1}")
  println(s"90% : LPI is ${yK - range2}, UPI is ${yK + range2}")

}
