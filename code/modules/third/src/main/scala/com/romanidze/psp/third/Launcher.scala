package com.romanidze.psp.third

import com.romanidze.psp.third.console.ConsoleWork

object Launcher extends App {
  ConsoleWork.launch()
}
