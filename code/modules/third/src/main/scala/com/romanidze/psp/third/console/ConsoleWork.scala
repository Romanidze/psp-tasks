package com.romanidze.psp.third.console

import java.util.concurrent.atomic.AtomicReference

import better.files.File
import com.romanidze.psp.third.files.FilesOperations

object ConsoleWork {

  def launch(): Unit = {

    val scanner = new java.util.Scanner(System.in)

    print("Write file name: ")

    val fileName: String = scanner.nextLine()

    val fileInput: File = FilesOperations.getFile(fileName)

    println()
    print("Please, write the operation to perform (available: read, write, quit, modify): ")

    val atomicOperation: AtomicReference[String] = new AtomicReference[String]()
    atomicOperation.set(scanner.nextLine())

    while (!atomicOperation.get().equals("quit")) {

      if (atomicOperation.get().equals("read")) {

        val numbers: Vector[String] = FilesOperations.readFile(fileInput)
        numbers.foreach(println)

      } else if (atomicOperation.get().equals("write")) {

        print("Please, write the input string with numbers to write: ")

        val writeInput: String = scanner.nextLine()

        FilesOperations.writeToFile(fileInput, writeInput)

      } else if (atomicOperation.get().equals("modify")) {

        print("Before modifying the data: do you want to write the result to new file or not?")

        val userCond: String = scanner.nextLine()

        val modifiedFileName: String = userCond match {

          case "yes" => s"${fileName}-new"
          case _     => fileName

        }

        val numbers: Vector[String] = FilesOperations.readFile(fileInput)

        numbers.zipWithIndex.foreach(elem => {

          val numberValue: String = elem._1
          val indexValue: Int = elem._2

          println(s"Your current number is: ${numberValue}")
          val commands: String = "(a - accept, r - replace, d - delete, i - insert new number)"

          print(s"Write, what you want to do with this number ${commands}: ")

          val commandInput: String = scanner.nextLine()

          commandInput match {

            case "a" => println("You accepted the number")
            case "r" => {

              print(s"Please, write the new value to replace ${numberValue}: ")
              val newValue: String = scanner.nextLine()

              val newVector: Vector[String] = numbers.updated(indexValue, newValue)
              val fileInput: File = FilesOperations.getFile(modifiedFileName)

              FilesOperations.writeToFile(fileInput, newVector.mkString(" "))

            }

            case "d" => {

              val newVector: Vector[String] = numbers.diff(Seq(numberValue))

              val fileInput: File = FilesOperations.getFile(modifiedFileName)

              FilesOperations.writeToFile(fileInput, newVector.mkString(" "))

            }

            case "i" => {

              print("Please, write the new value to insert: ")
              val newValue: String = scanner.nextLine()
              val newVector: Vector[String] = numbers.appended(newValue)

              val fileInput: File = FilesOperations.getFile(modifiedFileName)

              FilesOperations.writeToFile(fileInput, newVector.mkString(" "))

            }

            case _ => "No such command, please write again"

          }

        })

      }

      print("Please, write next command of what to do: ")

      atomicOperation.set(scanner.nextLine())

      println()

    }

    println("Thank you for using such program")

  }

}
