package com.romanidze.psp.second

import java.nio.file.Paths
import better.files.File
import scala.collection.mutable.ListBuffer

object Launcher extends App {

  val scanner = new java.util.Scanner(System.in)

  print("Write path to analyse: ")

  val filePath: String = scanner.nextLine()
  println()

  val inputDir = File(Paths.get(filePath))

  val files: Iterator[File] = inputDir.list

  val wordsList: ListBuffer[String] = ListBuffer.empty[String]

  files.foreach(elem => {

    val stringIter: Iterator[String] = elem.lineIterator
    stringIter.foreach(elem1 => wordsList += elem1.trim)

  })

  val finalLines: List[String] = wordsList.filter(!_.isEmpty).toList

  val keyWords: Seq[String] = Seq("val", "var", "def", "+", "-", "*", "/", "return")

  val functions: Seq[String] = Seq(
    "min",
    "max",
    "filter",
    "flatten",
    "diff",
    "intersect",
    "union",
    "map",
    "flatMap",
    "foldLeft",
    "foldRight",
    "drop",
    "head",
    "mkString"
  )

  val keywordCounter: Int = finalLines.foldLeft(0)((counter, inputStr) => {

    if (keyWords.contains(inputStr)) {
      counter + 1
    } else {
      counter
    }

  })

  val functionsCounter: Int = finalLines.foldLeft(0)((counter, inputStr) => {

    if (functions.contains(inputStr)) {
      counter + 1
    } else {
      counter
    }

  })

  val finalResult: Int = keywordCounter + functionsCounter

  println(s"LOC for inputpath ${filePath} : $finalResult")

}
