
import sbt._

object Dependencies {

  private val betterFiles: Seq[ModuleID] = Seq(
    "com.github.pathikrit" %% "better-files" % Versions.betterFiles
  )

  val firstTaskDeps: Seq[ModuleID] = betterFiles
  val secondTaskDeps: Seq[ModuleID] = betterFiles
  val thirdTaskDeps: Seq[ModuleID] = betterFiles
  val fourthTaskDeps: Seq[ModuleID] = betterFiles
  val sixthTaskDeps: Seq[ModuleID] = betterFiles
  val seventhTaskDeps: Seq[ModuleID] = betterFiles
  val eighthTaskDeps: Seq[ModuleID] = betterFiles
  val ninethTaskDeps: Seq[ModuleID] = betterFiles
  val tenthTaskDeps: Seq[ModuleID] = betterFiles

}
