# psp-tasks
Tasks for the PSP course

Each task is in separate module in ```modules``` folder

A1 assessment is in path "code/modules/first"

A2 assessment is in path "code/modules/second"

A3 assessment is in path "code/modules/third"

A4 assessment is in path "code/modules/fourth"

A6 assessment is in path "code/modules/sixth"

A7 assessment is in path "code/modules/seventh"

A8 assessment is in path "code/modules/eighth"

A9 assessment is in path "code/modules/nineth"

A10 assessment is in path "code/modules/tenth"
